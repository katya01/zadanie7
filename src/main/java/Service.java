import java.io.*;
import java.util.Scanner;

public class Service {

    public static void burnBinaryStreamArray(DataOutputStream out, int[] array) throws IOException {
        for (int i = 0; i < array.length; i++) {
            out.writeInt(array[i]);
        }
        out.close();
    }

    public static int[] readBinaryStreamArray(DataInputStream in, int[] arrayAnswer) throws IOException {

        for (int i = 0; i < arrayAnswer.length; i++) {
            arrayAnswer[i] = in.readInt();
        }
        return arrayAnswer;
    }

    public static void writeArrayCharacterStream(Writer out, int[] array) throws IOException {
        PrintWriter prWr = new PrintWriter(out);
        for (int i = 0; i < array.length; i++) {
            prWr.print(array[i]);
            prWr.print(' ');
        }
        out.close();
    }

    public static int[] readArrayCharacterStream(Reader in, int[] arrayAnswer) throws IOException {

        Scanner scanner = new Scanner(in);
        for (int i = 0; i < arrayAnswer.length; i++) {
            arrayAnswer[i] = scanner.nextInt();
        }
        return arrayAnswer;
    }

    public static int[] readArrayFromGivenPosition(RandomAccessFile rAF, int[] s,int pointer) throws IOException {
        try {
            rAF.seek(pointer * 4);
        } catch (IOException e) {
            System.err.println("Pointer error" + e);
        }
        if (pointer <= rAF.length() / 4) {
            int len=(int) (rAF.length() - rAF.getFilePointer()) / 4;
            for (int i = 0; i < len; i++) {
                s[i]=rAF.readInt();
            }

            return s;
        } else {
            throw new IllegalArgumentException("Pointer went beyond");
        }

    }

    public static String[] getListOfAllFiles(File file, String exten) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                file.setWritable(false);

                String[] files = file.list(new FileFilterExtends(exten));
                return files;
            } else {
                throw new IllegalArgumentException("File is not directory");
            }
        } else {
            throw new IllegalArgumentException("File does not exit");
        }

    }


}
